// Importing libraries
import java.util.Arrays;
import java.util.Random;
/**
 * MyLibrary class which contains books and accesses Book and Author for selection
 * and retrieval of information
 * @author Ron Friedman
 * @since 2020-09-23
 * @version 1.8.3
 */
public class MyLibrary {
    private Book[] books;
    
    /**
     * Default constructor
     * @param n index value
     */
    public MyLibrary(int n) {
        this.books = new Book[n];
    }
    /**
     * Constructor with all data members
     * @param book a book selected from Book array
     */
    public MyLibrary(Book[] book) {
        this.books = book;
    }
    /**
     * Copy constructor 
     * @param b MyLibrary constructor
     */
    public MyLibrary(MyLibrary b) {
        this.books = b.books;
    }
    /**
     * Selects a book for the user
     * @return books[r]
     */
    public Book selectBook() {
        Random rand = new Random();
        int r = rand.nextInt(books.length);
        return books[r];
    }
    /**
     * Selects a random book for user if idx is not valid
     * @param idx
     * @return books[idx] || selectBook()
     */
    public Book selectBook(int idx) {
        if(idx != books.length) {
            return selectBook();
        } else {
            return books[idx];
        }
    }
    /**
     * Calculates total price of all books inside array
     * @return sum
     */
    public double calcTotalPrice() {
        double sum = 0;
        for (Book book : books) {
            sum += book.getPrice();
        }
        return sum;
    }
    /**
     * Counts the amount of authors per book given a nationality
     * @param nationality
     * @return authorCount the amount of authors that have given nationality
     */
    public int countAuthorNum(String nationality) {
        int authorCount = 0;
        Author author = new Author();
        if (author.getNationality().equals(nationality))
            authorCount++;
        return authorCount;
    }
    /**
     * Compares if object is equal
     * @param obj
     * @return validation
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MyLibrary other = (MyLibrary) obj;
        if (!Arrays.deepEquals(this.books, other.books)) {
            return false;
        }
        return true;
    }
    /**
     * Output to string method
     * @return books contained inside Books[] array
     */
    @Override
    public String toString() {
        return String.format("MyLibrary: %10s\n", Arrays.toString(books));
    }
    /**
     * Getter and setter
     * @return books
     */
    public Book[] getBooks() {
        return books;
    }
    public void setBooks(Book[] books) {
        this.books = books;
    }
}
