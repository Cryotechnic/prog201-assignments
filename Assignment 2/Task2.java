/**
 * Generates an array with uniformly distributed numbers based on 
 * given variables
 * @author Ron Friedman
 * @version 1.1.1
 * @since 2020-09-22
 */
public class Task2 {
<<<<<<< HEAD
    /**
     * Generation method
     * @param startNum the starting number in array
     * @param endNum the ending number in array
     * @param amount the amount of values the user would like to be generated
     * @return uniformDistArr
     */
    public double[] generateUniformDistributionNum(double startNum, double endNum, int amount){
        double [] uniformDistArr = new double[amount]; // Array init
        uniformDistArr[0] = startNum; // startNum = index 0
        if (startNum > endNum) { // checks if startNum > endNum & swaps if req.
            double temp = startNum;
            startNum = endNum;
            endNum = temp;
        }
        // calculates distance (hop between numbers)
        double dist = ((endNum - startNum) / (amount - 1)); 
        // For loop goes through array and modifies each value so that it matches a unified dist
        for (int i = 1; i < amount; i++) {
            uniformDistArr[i] = ((uniformDistArr[i - 1] + dist) * 10) / 10.0;
        }
        return uniformDistArr;
=======
    public double[] generateUniformDistributionNum(double startNum, double endNum, int amount) {
        double div = 10.0;
        double[] uniformDistArr = new double[amount];
        uniformDistArr[0] = startNum;

        double dist = ((endNum - startNum)) / (amount - 1);
        for (int i = 1; i < amount; i++) {
            uniformDistArr[i] = ((uniformDistArr[i - 1] + dist) * 10) / div;
        }
        return uniformDistArr;   
>>>>>>> master
    }
    
    
}
