// Importing libraries
import java.util.Objects;
/**
 * Author class which contains multiple variables that describe the author
 * @author Ron Friedman
 * @since 2020-09-23
 * @version 1.6.4
 */
public class Author {
    private String name;
    private String nationality;
    private String email;
    /**
     * Default constructor
     */
    public Author() {
        this.name = "";
        this.email = "";
        this.nationality = "";
    } 
    /**
     * Constructor with author name, author nationality & author email
     * @param name
     * @param nationality
     * @param email 
     */
    public Author(String name, String nationality, String email) {
        this.name = name;
        this.nationality = nationality;
        this.email = email;
    }
    /**
     * Copy constructor, copies name, email, nationality into a new constructor
     * @param author 
     */
    public Author(Author author) {
        this.name = author.name;
        this.email = author.email;
        this.nationality = author.nationality;
    }    
    /**
     * Checks if nationality entered matches required nationality
     * @param nationality
     * @return validation
     */
    public boolean isNationalityValid(String nationality) {
        return nationality.equals("Canadian") || nationality.equals("American") ||
                nationality.equals("French") || nationality.equals("British") ||
                nationality.equals("German");
    }  
    /**
     * Checks if Object value is equal
     * @param obj
     * @return validation
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Author other = (Author) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.nationality, other.nationality)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        return true;
    }   
    /**
     * Output to user string
     * @return string
     */
    @Override
    public String toString() {
        return String.format("Author name: %s\nAuthor Nationality: %s\nAuthor "
                + "Email: ", name, nationality, email);
    }
    /**
     * Getters and setters
     * @return name, nationality, email
     */
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getNationality() {
        return nationality;
    }
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }   
}
