// Importing libraries
import java.util.Arrays;
import java.util.Objects;

/**
 * Book class which contains information regarding the book
 * @author Ron Friedman
 * @since 2020-09-23
 * @version 1.5.3
 */
public class Book {
    private String title;
    private String[] authors;
    private double price;

    /**
     * Default constructor
     * @param n index value
     */
    public Book(int n) {
        this.title = "";
        this.authors = new String[n];
        this.price = 0;
    }    
    /**
     * Constructor with all data members
     * @param title the title of the book
     * @param authors the author(s) of the book
     * @param price the price of the book
     */
    public Book(String title, String[] authors, double price) {
        this.title = title;
        this.authors = authors;
        this.price = price;
    }
    /**
     * Deep copy constructor
     * @param book book from Book[]
     */
    public Book(Book book) {
        this.title = book.title;
        this.authors = book.authors;
        this.price = book.price;
    }
    /**
     * Checks if object is equal
     * @param obj
     * @return validation
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Book other = (Book) obj;
        if (Double.doubleToLongBits(this.price) != Double.doubleToLongBits(other.price)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Arrays.deepEquals(this.authors, other.authors)) {
            return false;
        }
        return true;
    }
    /**
     * Outputs string to user
     * @return string 
     */
    @Override
    public String toString() {
        return "Book{" + "title=" + title + ", authors=" + authors + ", price=" + price + '}';
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String[] getAuthors() {
        return authors;
    }
    public void setAuthors(String[] authors) {
        this.authors = authors;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
}
