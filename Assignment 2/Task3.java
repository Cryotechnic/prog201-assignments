// Importing libraries
import java.util.Arrays;
/**
 * Calculates a score from judges
 * @author Ron Friedman
 * @version 1.2.0
 * @since 2020-09-21
 */
public class Task3 {
    
    /**
     * Calculates minimum value in array
     * @param score
     * @return minValue
     */
    public double min(double[] score){
        Arrays.sort(score); // sorts by ascending (small to big)
        double minValue = score[0];
        return minValue;
    }

    /**
     * Calculates the maximum value in array
     * @param score
     * @return maxValue
     */
    public double max(double[] score){
        Arrays.sort(score);
        double maxValue = score[0];
        return maxValue;
    }
    /**
     * Returns the final calculated score from score array
     * @param score
     * @return finalScore
     */
    public double calcFinalScore(double[] score) {
        double calcScore = 0;
        for (double num : score) {
            calcScore += num; // total sum of all given scores 
        }
        double finalScore = calcScore - min(score) - max(score) / score.length;
        return finalScore;
    }
}
