// Importing libraries
import java.util.Arrays;
import org.apache.commons.lang3.ArrayUtils;
/**
 * Basic arrays exercises
 * @author Ron Friedman
 * @since 2020-09-21
 * @version 1.2.7
 */
public class Task1 {
    /**
     * Calculates the sum of all values in array
     * @param nums array of numbers
     * @return sum of values
     */
    public static double sum(double[] nums) {
        return Arrays.stream(nums).sum();
    }
    /**
     * Calculates the average of all values in array
     * @param nums array of numbers
     * @return average of values
     */
    public static double avg(double[] nums) {
        return Arrays.stream(nums).average().orElse(Double.NaN);
    }
    /**
     * Finds the minimum value in array
     * @param nums array of numbers
     * @return min value
     */
    public static double min(double[] nums) {
        return Arrays.stream(nums).min().orElse(Double.NaN);
    }
    /**
     * Increases all values by given value
     * @param nums array of numbers
     * @param value how much to increase each number by
     */
    public static void increaseValue(double[] nums, double value) {
        for (int i = 0; i < nums.length; i++) {
            nums[i] += value;
        }
    }
    /**
     * Deletes value from given index
     * @param nums array of numbers
     * @param idx index in array
     */
    public static void delete(double[] nums, int idx) {
        ArrayUtils.remove(nums, idx);
    }
}
