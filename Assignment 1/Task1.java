/**
 * Method that reverses an integer number
 * @author Ron Friedman, 1926133
 * @version 1.0.5.2
 * @since 2020-09-08
 * TODO: ADD COMMENTS
 */

// Importing libraries
import java.util.Scanner;

  public class Task1 {
    public static void main(String[] args) {
      int num = 0;
      int revNum = 0;
      System.out.println("Please enter a valid integer value");
      Scanner in = new Scanner(System.in);
      num = in.nextInt();
      while(num != 0)
      {
        revNum *= 10;
        revNum += num % 10;
        num /= 10;
      }
      System.out.println("Reverse of inputted number: " + revNum);
    }

  }
