/**
 * Collection of methods that displays address information
 * @author Ron Friedman, 1926133
 * @version 1.5.2
 * @since 2020-09-13
 * TODO: Main class code
 */

 // Imporing libraries
 import java.util.regex.Pattern;


 public class Address {
    private String street;
    private int streetNum;
    private String city;
    private int zipCode;


    /**
     * Default constructor
     */
    public Address() {
        this.street = "";
        this.streetNum = 0;
        this.city = "";
        this.zipCode = 0;
    }

    /**
     * Constructor with address information (street, house number, city and zip code)
     * @param street Name of the street
     * @param streetNum House number on the street 
     * @param city Name of the city
     * @param zipCode Value of zipcode, corresponding to region where individual lives 
    */
    public Address(String street, int streetNum, String city, int zipCode) {
        this.street = street;
        this.streetNum = streetNum;
        this.city = city;
        this.zipCode = zipCode;
    }

    /**
     * Checks if streetNum is valid
     * @param streetNum House/building number
     * @return validation
     */
    public boolean isStreetNumValid(int streetNum) {
        return 0 <= streetNum && streetNum <= 99999;
    }

    /**
     * Checks if city name is valid
     * @param city
     * @return validation
     */
    public boolean isCityValid(String city) {
        boolean cond = true;
        if(city == null || city.isEmpty()) {
            return false;
        }
        for(int i = 0; i < city.length(); i++) {
            char cityChar = city.charAt(i);
            if(!Character.isLetter(cityChar) && cityChar != ' ' && cityChar != '-') {
                return false;
            }
        }
        return cond;
    }

    /**
     * Checks if zip code respects syntax (is valid)
     * @param zipCode
     * @return validation
     */
    public Boolean isZipCodeValid(String zipCode) {
        boolean cond;

        if(zipCode.length() != 6) {
            cond = false;
        }
        if (Pattern.matches("^[A-Z]\\d[A-Z]\\d[A-Z]\\d$", zipCode)) {
            cond = true;
        } else {
            cond = false;
        }
        return cond;
    } 
    /**
     * Checks if Address value is equal 
     * @param Address
     * @return validation
     */
    @Override
    public boolean equals(Address address) {
        if (this == address) {
            return true;

            if (address == null || getClass != address.getClass()) {
                return false;
            }
        }
    }

    public String toString(String street, int streetNum, String city, String zipCode) {
        System.out.printf("Street: %d, %s", int streetNum, String street);
        System.out.println("City: " + city);
        System.out.println("Zip Code (Postal Code): " + zipCode);
    }
    public String toCamelCase(String str) {
        String[] strArr = str.split(" ");
        for (String strCamel : strArr) {
            Character.toUpperCase(strCamel.charAt(0));
        }
    }





 }