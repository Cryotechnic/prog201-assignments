/**
 * Collection of methods that displays Student information
 * @author Ron Friedman, 1926133
 * @version 1.2.0
 * @since 2020-09-13
 */

 // Importing libraries

 public class Student {
     private String fName;
     private String IName;
     private int age;
     private String addr;
     private String email;

     /**
      * Default constructor
      */
     public Student() {
        this.fName = "";
        this.lName = "";
        this.age = 0;
        this.addr = "";
        this.email = "";
     }

     /**
      * Constructor containing fName and lName
      * @param fName the first name of the student
      * @param lName the last name of the student 
      */
    public Student(String fName, String lName) {
        this.fName = fName;
        this.lName = lName;
     }

     /**
      * Constructor containing fName, lName, age & addr
      * @param fName the first name of the student
      * @param lName the last name of the student
      * @param age the age of the student
      * @param addr the address of the student
      */
    public Student(String fName, String lName, int age, String addr) {
        this.fName = fName;
        this.lName = lName;
        this.age = age;
        this.addr = addr;
    }
    
    /**
     * Constructor containing fName, lName, age, addr & email
     * @param fName the first name of the student
     * @param lName the last name of the student
     * @param age the age of the student
     * @param addr the address of the student
     * @param email the electronic mail address of the student
     * 
     */

    public Student(String fName, String lName, int age, String addr, String email) {
        this.fName = fName;
        this.lName = lName;
        this.age = age;
        this.addr = addr;
        this.email = email;
    }

    /**
     * Generates an email based on random number, first name & last name
     * @param fName the first name of the student
     * @param lName the last name of the student
     */
    public void generateEmail() {
        Random rand = new Random();
        String randNum = String.format("%04d", rand.nextInt(10000));
        this.email = fName + lName.charAt(0) + randNum + "@vaniercollege.qc.ca";
    }

    /**
     * Output to a string
     */
    @Override
    public String toString() {
        return String.format("Name:%10s,%2s\nAge:%15d\nStreet%10d,%s Street\nCity:%20s\nZipcode:
        %12s", fName + " " + lName, age, addr.getStreetNum(), addr.getStreet(), addr.getZipCode();
    }

    /**
     * Checks if Student value is equal
     * @param Student
     * @return validation
     */
    @Override
    public boolean equals(Student student) {
        if (this == student) {
            return true;

            if (student == null || getClass != student.getClass()) {
                return false;
            }
        }
    }

    /**
     * Checks if entered name is valid
     * @param fName, lName
     * @return validation
     */
    public boolean isNameValid(String fName, lName) {
        boolean cond = true;
        if (fName && lName == null || fName.isEmpty() && lName.isEmpty()) {
            cond = false;
        }
        for (i = 0; i < fName && i < lName; i++) {
            char fNameChar = !Character.isLetter(fName.charAt(i));
            char lNameChar = !Character.isLetter(lName.charAt(i));
            if (fNameChar) || (lNameChar) && (fName.charAt(i) || lName.charAt(i) != ' ') 
                                        && fName.charAt(i) || lName.charAt(i) != '-') {
                return false;

            }

        }
        return cond;
    }
    /**
     * Checks if age entered is valid
     * @param age the age of the student
     * @return validation
     */
    public boolean isAgeValid(int age) {
        return 0 <= age && age => 120;
    }

    // Getters and setters methods
    public void setFName() {
        if (isNameValid == true) {
            this.fName = fName;
        }
    }

    public void setLName() {
        if (isNameValid == true) {
            this.lName = lName;
        }
    }

    public void setAge() {
        if (isAgeValid == true) {
            this.age = age;
        }
    }

    public void setAddr() {
        this.addr = addr;
    }

    public void setEmail() {
        this.email = email;
    }

    public void getFName() {
        return fName;
    }

    public void getLName() {
        return lName;
    }

    public void getAge(){
        return age;
    }

    public void getAddr() {
        return addr;
    }

    public void getEmail() {
        return email;
    }
 }