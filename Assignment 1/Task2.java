/**
 * Method that counts the score of a String
 * @author Ron Friedman, 1926133
 * @version 1.2.1
 * @since 2020-09-11
 * TODO: Add comments
 */

 // Importing libraries
 import java.util.Scanner;

public class Task2 { 
    public static void main(String[] args) {
        System.out.println("Enter a valid string: ");
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        System.out.printf("Your string achieved a score of %.2f", new Task2().stringCount(str));
    }

    public double stringCount(String str) {
        double score = 0;
        if(str == null || str.isEmpty()) {
            return score;
        }

        for (int i = 0; i < str.length(); i++) {
            if (Character.isLetter(str.charAt(i))) {
                score += 2;
            } else if (Character.isDigit(str.charAt(i))) {
                score += 1;
            } else if (str.charAt(i) != ' ') {
                score += 3;
            }
        
        }
        return score / str.length();
    }
}
